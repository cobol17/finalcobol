       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRADER3.
       AUTHOR. PANUWAT.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADER-FILE ASSIGN TO "trader3.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT TRADER3-FILE ASSIGN TO "trader3.rpt"
              ORGANIZATION IS SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  TRADER3-FILE.
       01 PRINT-LINE         PIC X(99).

       FD  TRADER-FILE.
       01 TRADER-REC.
           88 END-OF-TRADER-FILE         VALUE HIGH-VALUE.
       05 CITY-ID            PIC X(2).
           05 TRADER-ID      PIC X(4).
           05 TRADER-MONEY   PIC X(6).
       WORKING-STORAGE SECTION. 
       01  PAGE-HEADER.
           05 FILLER         PIC X(77)
                                         VALUE
                  "TRADER HIGH MONEY".
       01  PAGE-FOOTING.
           05 FILLER         PIC X       VALUE SPACES.
           05 FILLER         PIC X(5)    VALUE "PAGE:".
           05 PRN-PAGE-NUM   PIC Z9.
       01  COLUMN-HEADING    PIC X(41)
                                         VALUE
                  "CityID      TraderID     Money".
       01  TRADER-DETAIL-LINE.
           05 FILLER            PIC X       VALUE SPACES.
           05 PRN-CITY-ID       PIC X(5).
           05 FILLER            PIC X(4)    VALUE SPACES.
           05 PRN-TRADER-ID     PIC X(20).
           05 FILLER            PIC XX      VALUE SPACES.
           05 PRN-TRADER-MONEY  PIC X.
           05 FILLER            PIC X(4)    VALUE SPACES.
       01  LINE-COUNT           PIC 99      VALUE ZEROES.
           88 NEW-PAGE-REQIRED              VALUE 40 THRU 99.
       01  PAGE-COUNT           PIC 99      VALUE ZEROS.
       PROCEDURE DIVISION 

       PROCESS-TRADER3.
           OPEN INPUT TRADER-FILE
           OPEN OUTPUT TRADER3-FILE
           PERFORM READ-TRADER-FILE
           PERFORM PROCESS-PAGE UNTIL END-OF-TRADER-FILE 

           CLOSE TRADER-FILE ,TRADER3-FILE 
           GOBACK
           .
       
       PROCESS-PAGE.
           PERFORM WRITE-HEADING
           PERFORM PROCES-DETAIL UNTIL END-OF-TRADER-FILE
              OR NEW-PAGE-REQIRED
           PERFORM WRITE-FOOTER
           .
       
       WRITE-HEADING.
       WRITE PRINT-LINE FROM PAGE-HEADER AFTER ADVANCING PAGE
       WRITE PRINT-LINE FROM COLUMN-HEADING AFTER ADVANCING 2 LINES
       COMPUTE LINE-COUNT = LINT-COUNT + 3
       COMPUTE PAGE-COUNT = PAGE-COUNT + 1
       MOVE ZEROS TO LINE-COUNT
       .
